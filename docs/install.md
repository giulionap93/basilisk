


@page bskInstall Installing Basilisk


The following links provide information on how to install Basilisk.  Note that BSK is currently supported on Linux, macOS and Windows.  Further, the software requires a Python installation and some support packages to be installed.  Be careful to read the installation instructions carefully.  Other links provide information on optional packages and Basilisk capabilities, as well as how to generate the documentation.
- @ref installLinux        "Linux Installation"
- @ref installMacOS        "macOS Installation"
- @ref installWindows      "Windows Installation"
- @ref installOptionalPackages "Installing Optional Python Packages"
- @ref cmakeOptions         "Cmake Flag Options"
- @ref customPython         "Using a Non-Default Python Configuration Python 3 and 2"
