


@page bskTutorials Basilisk Tutorials
@tableofcontents



@section chOrbits Orbital Simulations

-# @ref scenarioBasicOrbitGroup        "Basic Orbits"
-# @ref scenarioIntegratorsGroup       "Choosing Integrators"
-# @ref scenarioOrbitManeuverGroup     "Doing Impulsive Orbit Maneuvers"
-# @ref scenarioOrbitMultiBodyGroup    "Using Multiple Gravitational Bodies"
-# @ref scenarioCentralBodyGroup       "Setting States Relative to Planets"
-# @ref scenarioPatchedConicGroup      "Simulating a Patched Conic Solution"


@section chAttitude Attitude Simulations
-# <b>Attitude Regulation Control</b>
    -# @ref scenarioAttitudeFeedbackGroup      "Using 1 Task Group"
    -# @ref scenarioAttitudeFeedback2TGroup    "Using 2 Task Groups (SIM and FSW)"
    -# @ref scenarioAttitudePythonPDGroup      "Using Python FSW Module"
    -# @ref scenarioAttitudePointingGroup      "Basic Attitude Control in Deep Space"
    -# @ref scenarioAttitudeFeedbackNoEarthGroup  "Complex Control in Deep Space"

-# <b>Attitude Guidance</b><br>
    -# @ref scenarioAttitudeGuidanceGroup      "Hill Frame Guidance"
    -# @ref scenarioAttGuideHyperbolicGroup    "Velocity Frame Guidance"
-# @ref scenarioAttitudeFeedbackRWGroup      "Reaction Wheel Control"
-# @ref scenarioAttitudeFeedback2T_THGroup   "Thrusters Control"<br>
-# @ref scenarioAttitudeSteeringGroup        "MRP Steering Attitude Control"



@section chComplexSC Complex Spacecraft Dynamics Simulations
-# @ref scenarioFuelSloshGroup                 "Fuel Slosh"
-# @ref scenarioHingedRigidBodyGroup           "Flexible (Hinged) Panels"


@section chPlanetEnv Planetary Environments
-# <b> Magnetic Field Models</b>
    -# @ref scenarioMagneticFieldCenteredDipoleGroup   "Centered Dipole Model"
    -# @ref scenarioMagneticFieldWMM                   "World Magnetic Model WMM"

@section chScSensors Spacecraft Sensors
-# <b>Coarse Sun Sensors</b><br>
    -# @ref scenarioCSSGroup                  "Adding CSS to simulation"
    -# @ref scenarioCSSFiltersGroup           "Estimating Sun Heading with CSS"

@section chMcSim Monte Carlo Simulations
-# @ref scenarioMonteCarloAttRWGroup                   "MC run with RW control"
-# @ref scenarioMonteCarloSpiceGroup                   "MC run using Spice within the Python simulation setup"


@section chBskSim bskSim()-Based Simulation
-# @ref scenario_BasicOrbitGroup                       "Basic Orbital Simulation"
-# @ref scenario_FeedbackRWGroup                       "Attitude Detumble Control"
-# @ref scenario_AttGuidanceGroup                      "Hill Pointing Attitude Control"
-# @ref scenario_AttGuidHyperbolicGroup                "Velocity Frame Pointing Control"
-# @ref scenario_AttSteeringGroup                      "MRP Steering Attitude Control"
-# @ref scenario_AttEclipseGroup                       "Sun Pointing Mode Include Eclipse Evaluation"


@section chSFF Spacecraft Formation Flying
-# <b>Formation Flying Dynamics</b>
    -# @ref scenario_BasicOrbitFormationGroup          "Two-Spacecraft Formation"
-# <b>Formation Flying Control</b><br>
    -# @ref scenario_RelativePointingFormationGroup   "Relative Pointing Control"

@section chLivePlotting Simulations Using Live Plotting
-# @ref scenarioBasicOrbitLiveGroup                 "Regular Basilisk simulation using Live Plotting"
-# @ref scenario_BasicOrbitLiveGroup                 "bskSim Basilisk simulation using Live Plotting"

@section chViz Simulations Interfacing with Vizard
-# @ref scenarioVizPointGroup                      "Pointing a Vizard Camera"

